package com.msk.produtosperigosos;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.msk.produtosperigosos.info.Simbologia;
import com.msk.produtosperigosos.info.SobreApp;
import com.msk.produtosperigosos.info.TelaInicial;
import com.msk.produtosperigosos.info.TelefonesUteis;
import com.msk.produtosperigosos.listas.ListaClasseDeRisco;
import com.msk.produtosperigosos.listas.ListaNrDeRisco;
import com.msk.produtosperigosos.listas.PesquisaProduto;

public class ProdutosPerigosos extends ActionBarActivity implements
		OnItemClickListener {

	private ListView menuNavegador;
	private String[] opcoesNavegador;
	private DrawerLayout navegador;
	private LayoutInflater inflater;
	private FrameLayout tela, tela0, tela1, tela2, tela3, tela4, tela5;
	private View titulo;
	Resources r = null;
	FragmentManager fm;
	Fragment fragmento, fragmento0, fragmento1, fragmento2, fragmento3,
			fragmento4, fragmento5;
	ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.navegador);
		r = getResources();

		Iniciador();
		usarActionBar();
		CarregaFragmentos();
		navegador.openDrawer(Gravity.LEFT);
		menuNavegador.setOnItemClickListener(this);
	}

	private void Iniciador() {
		// DEFINE OS ITENS DA TELA
		opcoesNavegador = r.getStringArray(R.array.opcoes_navegador);
		menuNavegador = (ListView) findViewById(R.id.lista_lateral);
		tela = (FrameLayout) findViewById(R.id.tela);
		tela0 = (FrameLayout) findViewById(R.id.tela0);
		tela1 = (FrameLayout) findViewById(R.id.tela1);
		tela2 = (FrameLayout) findViewById(R.id.tela2);
		tela3 = (FrameLayout) findViewById(R.id.tela3);
		tela4 = (FrameLayout) findViewById(R.id.tela4);
		tela5 = (FrameLayout) findViewById(R.id.tela5);

		inflater = (LayoutInflater) getApplicationContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		titulo = inflater.inflate(R.layout.titulo_barra, menuNavegador, false);
		menuNavegador.addHeaderView(titulo);

		navegador = (DrawerLayout) findViewById(R.id.barra_lateral);
		menuNavegador.setAdapter(new ArrayAdapter<String>(this,
				R.layout.item_navegador, opcoesNavegador));

	}

	private void CarregaFragmentos() {
		// PRE_CARREGAMENTO DOS FRAGMENTOS
		fragmento = new TelaInicial();
		fragmento0 = new ListaClasseDeRisco();
		fragmento1 = new ListaNrDeRisco();
		fragmento2 = new PesquisaProduto();
		fragmento3 = new TelefonesUteis();
		fragmento4 = new SobreApp();
		fragmento5 = new Simbologia();

		fm = getSupportFragmentManager();
		fm.beginTransaction().replace(R.id.tela, fragmento).commit();
		fm.beginTransaction().replace(R.id.tela0, fragmento0).commit();
		fm.beginTransaction().replace(R.id.tela1, fragmento1).commit();
		fm.beginTransaction().replace(R.id.tela2, fragmento2).commit();
		fm.beginTransaction().replace(R.id.tela3, fragmento3).commit();
		fm.beginTransaction().replace(R.id.tela4, fragmento4).commit();
		fm.beginTransaction().replace(R.id.tela5, fragmento5).commit();

		tela.setVisibility(View.VISIBLE);
		tela0.setVisibility(View.GONE);
		tela1.setVisibility(View.GONE);
		tela2.setVisibility(View.GONE);
		tela3.setVisibility(View.GONE);
		tela4.setVisibility(View.GONE);
		tela5.setVisibility(View.GONE);

	}

	@SuppressLint("NewApi")
	@Override
	public void onItemClick(AdapterView<?> barra, View v, int posicao, long arg3) {

		menuNavegador.setItemChecked(posicao, true);
		navegador.closeDrawer(barra);
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
			if (posicao > 0)
			actionBar.setTitle(opcoesNavegador[posicao - 1]);
		
		switch (posicao) {
		case 1:
			tela.setVisibility(View.GONE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.VISIBLE);
			break;
		case 2:
			tela.setVisibility(View.GONE);
			tela0.setVisibility(View.VISIBLE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);
			break;
		case 3:
			tela.setVisibility(View.GONE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.VISIBLE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);
			break;
		case 4:
			tela.setVisibility(View.GONE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.VISIBLE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);
			break;
		case 5:
			tela.setVisibility(View.GONE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.VISIBLE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);
			break;
		case 6:
			tela.setVisibility(View.GONE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.VISIBLE);
			tela5.setVisibility(View.GONE);
			break;
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {

			tela.setVisibility(View.VISIBLE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);
			
			usarActionBar();
			navegador.openDrawer(Gravity.LEFT);
			
		}
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			tela.setVisibility(View.VISIBLE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);

			Dialogo();
		}

		return super.onKeyDown(keyCode, event);
	}

	private void Dialogo() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				new ContextThemeWrapper(this, R.style.TemaDialogo));

		builder.setTitle(R.string.dica_sair);
		builder.setMessage(R.string.texto_sair);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogo, int id) {
				finish();
			}
		});
		builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogo, int id) {
				dialogo.dismiss();
				navegador.openDrawer(Gravity.LEFT);
				usarActionBar();
			}
		});
		// create alert dialog
		AlertDialog alertDialog = builder.create();
		// show it
		alertDialog.show();
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(false);
			actionBar.setTitle(R.string.app_name);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			tela.setVisibility(View.VISIBLE);
			tela0.setVisibility(View.GONE);
			tela1.setVisibility(View.GONE);
			tela2.setVisibility(View.GONE);
			tela3.setVisibility(View.GONE);
			tela4.setVisibility(View.GONE);
			tela5.setVisibility(View.GONE);
			usarActionBar();
			navegador.openDrawer(Gravity.LEFT);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
