package com.msk.produtosperigosos.info;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.msk.produtosperigosos.R;

public class DetalheProduto extends Activity {

	// ELEMENTOD DA TELA
	private TextView nrONU, nrClasse, nrRisco, nomeProduto, nomeClasse, nomeRisco, saude, incendio, epi, isolamento, evacuacao;
	private ImageView rotulo;
	
	// VETORES COM DADOS DOS PRODUTOS
	private String[] onu, nclasse, nrisco, produto, classe, risco, dsaude, dincendio, depi, disolamento, devacuacao;
	private int[] idRotulo;
	Resources r;
	
	// VARIAVES QUE SERAO UTILIZADAS
	private int nProduto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.detalhe_produto);
		r = getResources();
		// RECEBE NUMERO PRODUTO
		Bundle envelope = getIntent().getExtras();
		nProduto = envelope.getInt("nr");
		
		Inicando();
		usarActionBar();
		MostraInfoProduto(nProduto);
		DefineRotulo(nProduto);
	}

	private void Inicando() {

		// ELEMENTOS QUE SERAO EXIBIDOS NA TELA
		nrONU = (TextView) findViewById(R.id.tvNrONU);
		nrClasse = (TextView) findViewById(R.id.tvNrClasse);
		nrRisco = (TextView) findViewById(R.id.tvNrRisco);
		nomeProduto = (TextView) findViewById(R.id.tvNomeProduto);
		nomeClasse = (TextView) findViewById(R.id.tvNomeClasse);
		nomeRisco = (TextView) findViewById(R.id.tvNomeRisco);
		rotulo = (ImageView) findViewById(R.id.ivRotuloRisco);
		saude = (TextView) findViewById(R.id.tvPerigoSaude);
		incendio = (TextView) findViewById(R.id.tvPerigoIncendio);
		epi = (TextView) findViewById(R.id.tvNivelProtecao);
		isolamento = (TextView) findViewById(R.id.tvIsolamento);
		evacuacao = (TextView) findViewById(R.id.tvEvacuacao);
		
		
		// BUSCA DETALHES NO ARQUIVO XML
		onu = r.getStringArray(R.array.nr_onu);
		nclasse = r.getStringArray(R.array.nr_classe);
		nrisco = r.getStringArray(R.array.nr_risco);
		produto = r.getStringArray(R.array.nome_produto);
		classe = r.getStringArray(R.array.nome_classe_risco);
		risco = r.getStringArray(R.array.nome_nr_risco);
		dsaude = r.getStringArray(R.array.perigo_saude);
		dincendio = r.getStringArray(R.array.perigo_incendio);
		depi = r.getStringArray(R.array.nivel_de_protecao);
		disolamento = r.getStringArray(R.array.distancia_isolamento);
		devacuacao = r.getStringArray(R.array.distancia_evacuacao);
		
		// DEFINE OS ROTULOS QUE SERAO EXIBIDOS
		idRotulo = new int[] {
			R.drawable.rotulo_explosivo, // 1.1, 1.2, 1.3
			R.drawable.rotulo_explosivo_4, // 1.4
			R.drawable.rotulo_explosivo_5, // 1.5
			R.drawable.rotulo_explosivo_6, // 1.6
			R.drawable.rotulo_gas_inflamavel, // 2.1
			R.drawable.rotulo_gas_nao_toxico, // 2.2
			R.drawable.rotulo_gas_toxico, // 2.3
			R.drawable.rotulo_liquido_inflamavel, // 3
			R.drawable.rotulo_solido_inflamavel, // 4.1
			R.drawable.rotulo_combustao_expontanea, // 4.2
			R.drawable.rotulo_perigoso_quando_molhado, // 4.3
			R.drawable.rotulo_oxidante, // 5.1
			R.drawable.rotulo_peroxido_organico, // 5.2
			R.drawable.rotulo_toxico, // 6.1
			R.drawable.rotulo_nocivo, // 6.2
			R.drawable.rotulo_substancia_infectante, // 6.3
			R.drawable.rotulo_radioativo, // 7
			R.drawable.rotulo_corrosivo, // 8
			R.drawable.rotulo_substancias_diversas // 9
		};
		
	}

	private void MostraInfoProduto(int i) {

		nrONU.setText(r.getString(R.string.dica_nr_onu, onu[i]));
		nrClasse.setText(r.getString(R.string.dica_nr_classe, nclasse[i]));
		nrRisco.setText(r.getString(R.string.dica_nr_risco, nrisco[i]));
		nomeProduto.setText(r.getString(R.string.dica_nome_produto, produto[i]));
		nomeClasse.setText(r.getString(R.string.dica_nome_classe, classe[i]));
		nomeRisco.setText(r.getString(R.string.dica_nome_risco, risco[i]));
		saude.setText(r.getString(R.string.dica_saude, dsaude[i]));
		incendio.setText(r.getString(R.string.dica_incendio, dincendio[i]));
		epi.setText(r.getString(R.string.dica_protecao, depi[i]));
		isolamento.setText(r.getString(R.string.dica_isolamento, disolamento[i]));
		evacuacao.setText(r.getString(R.string.dica_evacuacao, devacuacao[i]));
		
		if (nrisco[i].equals("0")) {
			nrRisco.setVisibility(View.GONE);
			nomeRisco.setVisibility(View.GONE);
		}
	}


	private void DefineRotulo(int i) {
		
		if (nclasse[i].equals("9")){
			rotulo.setImageResource(idRotulo[18]);
		}
		if (nclasse[i].equals("8")){
			rotulo.setImageResource(idRotulo[17]);
		}
		if (nclasse[i].equals("7")){
			rotulo.setImageResource(idRotulo[16]);
		}
		if (nclasse[i].equals("6.3")){
			rotulo.setImageResource(idRotulo[15]);
		}
		if (nclasse[i].equals("6.2")){
			rotulo.setImageResource(idRotulo[14]);
		}
		if (nclasse[i].equals("6.1")){
			rotulo.setImageResource(idRotulo[13]);
		}
		if (nclasse[i].equals("5.2")){
			rotulo.setImageResource(idRotulo[12]);
		}
		if (nclasse[i].equals("5.1")){
			rotulo.setImageResource(idRotulo[11]);
		}
		if (nclasse[i].equals("4.3")){
			rotulo.setImageResource(idRotulo[10]);
		}
		if (nclasse[i].equals("4.2")){
			rotulo.setImageResource(idRotulo[9]);
		}
		if (nclasse[i].equals("4.1")){
			rotulo.setImageResource(idRotulo[8]);
		}
		if (nclasse[i].equals("3")){
			rotulo.setImageResource(idRotulo[7]);
		}
		if (nclasse[i].equals("2.3")){
			rotulo.setImageResource(idRotulo[6]);
		}
		if (nclasse[i].equals("2.2")){
			rotulo.setImageResource(idRotulo[5]);
		}
		if (nclasse[i].equals("2.1")){
			rotulo.setImageResource(idRotulo[4]);
		}
		if (nclasse[i].equals("1.6")){
			rotulo.setImageResource(idRotulo[3]);
		}
		if (nclasse[i].equals("1.5")){
			rotulo.setImageResource(idRotulo[2]);
		}
		if (nclasse[i].equals("1.4")){
			rotulo.setImageResource(idRotulo[1]);
		}
		if (nclasse[i].equals("1") || nclasse[i].equals("1.1") || nclasse[i].equals("1.2") || nclasse[i].equals("1.3")){
			rotulo.setImageResource(idRotulo[0]);
		}
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
