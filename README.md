ProdutosPerigosos
=================

Aplicativo para consulta de número da ONU e classe de risco de elementos classificados como produtos perigosos

Detalhes das versões:

1.0 - Consulta classes de risco;
1.1 - Consulta números de risco;
1.2 - Consulta produtos por número da ONU ou nome;
1.3 - Consulta os detalhes da classes de risco;
1.4 - Consulta os detalhes dos produtos;
1.4.1 - Utiliza a navegação por menu lateral;
